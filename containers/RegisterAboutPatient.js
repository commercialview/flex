import React, {Component} from 'react';
import {View,Image, Text, TouchableOpacity, AsyncStorage} from 'react-native';
import {DatePicker, Picker, Container, Item, Input } from 'native-base';
import {Actions} from 'react-native-router-flux';

import HeaderV from '../components/f_HeaderV.js';

type Props = {};
export default class RegisterAboutPatient extends Component<Props> {

    constructor(props) {
        super(props);
        this.state = { 
          chosenDate: new Date(),
          name: '',
          lastName:'',

        };
        this.setDate = this.setDate.bind(this);

      }
      setDate(newDate) {
        this.setState({ chosenDate: newDate });
      }

      componentDidMount(){
        this.setDate(new Date(1990,1,1));
      }

      save(){
        var aboutYou = {
          pname: this.state.name,
          plastName: this.state.lastName,
          date: this.state.chosenDate ,
        }
    
        AsyncStorage.setItem("Flexer:userDataAboutYouPatient", JSON.stringify(aboutYou));
        Actions.registeraboutpatientaddress({type:'reset'});
    }

  render() {
    return (
      <Container style={{backgroundColor:'#F7F7F7'}}>
        <HeaderV  />

        <View style={{paddingTop:20, paddingLeft:20, paddingRight:20}}>
          <Text style={{paddingLeft:2, fontSize:20, paddingTop:2, fontFamily:'SourceSansPro-Light',color:'#8DAC4A'}}>Acerca del paciente</Text>

          <Item style={{marginTop:10, padding:0, borderColor:'transparent'}}>
              <Input onChangeText={(text) => this.setState({name:text})}  style={{fontSize:14, fontFamily:'SourceSansPro-Light', backgroundColor:'white',borderRadius:10}} placeholder="Nombre" />
          </Item>
            <Item style={{marginTop:10, padding:0, borderColor:'transparent'}}>
              <Input onChangeText={(text) => this.setState({lastName:text})}  style={{fontSize:14, fontFamily:'SourceSansPro-Light', backgroundColor:'white',borderRadius:10}} placeholder="Apellido" />
          </Item>

          <Text style={{paddingLeft:2, fontSize:18, paddingTop:10, fontFamily:'SourceSansPro-Light',color:'#8DAC4A'}}>Fecha de nacimiento de paciente</Text>


          <DatePicker
            defaultDate={new Date(2018, 4, 4)}
            minimumDate={new Date(2018, 1, 1)}
            maximumDate={new Date(2018, 12, 31)}
            locale={"en"}
            timeZoneOffsetInMinutes={undefined}
            modalTransparent={false}
            animationType={"fade"}
            androidMode={"default"}
            placeHolderText="Select date..."
            placeHolderTextStyle={{ marginTop:10, fontFamily:'SourceSansPro-Light', backgroundColor:'white', borderRadius:10, color:'#7B7B7B' }}
            textStyle={{ marginTop:10, fontFamily:'SourceSansPro-Light', backgroundColor:'white', borderRadius:10, color:'#7B7B7B' }}
            onDateChange={this.setDate}
            disabled={false}
            />
        </View>

        <View style={{marginTop:40, paddingLeft:20, paddingRight:20}}>
          <TouchableOpacity onPress={() => this.save() } style={{position:'absolute', right: 10, width:80, height:80}}>
           <Image style={{position:'absolute', right: 10, width:70, height:70}} source={require('../res/emailconfirm/btn.png')} />
          </TouchableOpacity>
        </View>

      </Container>
    );
  }
}

