import React, {Component} from 'react';
import {View,Image, Text, TouchableOpacity} from 'react-native';
import { Container, Content } from 'native-base';
import {Actions} from 'react-native-router-flux';

import Header from '../components/f_Header.js';

type Props = {};
export default class Home extends Component<Props> {

  render() {
    return (
      <Container style={{backgroundColor:'#8DAC4B'}}>
        <Header bgcolor="#8DAC4B" />
        
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Image style={{alignSelf:'center',width:225, height:115}} source={require('../res/home/hello.png')} />
          <Text style={{fontSize:16, fontFamily:'SourceSansPro-Light',paddingTop:25, textAlign:'center', width: 240, color:'white'}}>Para ofrecerte información más precisa, te haremos algunas preguntas por única vez.</Text>
          <TouchableOpacity onPress={ () => Actions.login()}>
            <Image style={{marginLeft:45,alignSelf:'center',marginTop:100, width:225, height:92}} source={require('../res/home/tap.png')} />
          </TouchableOpacity>
        </View>

      </Container>
    );
  }
}

