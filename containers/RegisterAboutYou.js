import React, {Component} from 'react';
import {View,Image, Text, TouchableOpacity, AsyncStorage} from 'react-native';
import {Icon, Picker, Container, Item, Input } from 'native-base';
import {Actions} from 'react-native-router-flux';

import HeaderV from '../components/f_HeaderV.js';

type Props = {};
export default class RegisterAboutYou extends Component<Props> {

  constructor(props) {
    super(props);

    this.state = {
      selected: "key0",
      name: '',
      lastName:'',
      relationship: '',
      phone:''
    };

  }
  onValueChange(value: string) {
    this.setState({
      selected: value
    });
  }

  save(){

    var aboutYou = {
      name: this.state.name,
      lastName: this.state.lastName,
      relationship: this.state.selected ,
      phone: this.state.phone
    }

    AsyncStorage.setItem("Flexer:userDataAboutYou", JSON.stringify(aboutYou));

    Actions.registeraboutpatient({type:'reset'});

  }
  

  render() {
    return (
      <Container style={{backgroundColor:'#F7F7F7'}}>
        <HeaderV  />

        <View style={{paddingTop:20, paddingLeft:20, paddingRight:20}}>
          <Text style={{paddingLeft:2, fontSize:20, paddingTop:2, fontFamily:'SourceSansPro-Light',color:'#8DAC4A'}}>Acerca de vos (no del paciente)</Text>

          <Item style={{marginTop:10, padding:0, borderColor:'transparent'}}>
              <Input onChangeText={(text) => this.setState({name:text})}  style={{fontSize:14, fontFamily:'SourceSansPro-Light', backgroundColor:'white',borderRadius:10}} placeholder="Juan" />
          </Item>
            <Item style={{marginTop:10, padding:0, borderColor:'transparent'}}>
              <Input  onChangeText={(text) => this.setState({lastName:text})}  style={{fontSize:14, fontFamily:'SourceSansPro-Light', backgroundColor:'white',borderRadius:10}} placeholder="Pérez" />
          </Item>

          <Text style={{paddingLeft:2, fontSize:18, paddingTop:10, fontFamily:'SourceSansPro-Light',color:'#8DAC4A'}}>Relación con el paciente</Text>

          <Picker
              mode="dropdown"
              iosHeader="Select your SIM"
              iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#8DAC4A", fontSize: 25 }} />}
              style={{ width: undefined, borderRadius:10 }}
              selectedValue={this.state.selected}
              itemStyle={{
                backgroundColor:'white',borderRadius:10
              }}
              itemTextStyle={{ color:'red', backgroundColor:'white',borderRadius:10
            }}

              onValueChange={this.onValueChange.bind(this)}
            >
              <Picker.Item label="Madre" value="key0" />
              <Picker.Item label="Padre" value="key1" />
              <Picker.Item label="Definir el resto" value="key2" />
            </Picker>

          <Text  style={{paddingLeft:2, fontSize:18, paddingTop:10, fontFamily:'SourceSansPro-Light',color:'#8DAC4A'}}>Número de teléfono</Text>

          <Item style={{marginTop:10, padding:0, borderColor:'transparent'}}>
              <Input  onChangeText={(text) => this.setState({phone:text})} style={{fontSize:14, fontFamily:'SourceSansPro-Light', backgroundColor:'white',borderRadius:10}} placeholder="54 91136905203" />
          </Item>

        </View>


        <View style={{marginTop:40, paddingLeft:20, paddingRight:20}}>
          <TouchableOpacity onPress={() => this.save() } style={{position:'absolute', right: 10, width:80, height:80}}>
           <Image style={{position:'absolute', right: 10, width:70, height:70}} source={require('../res/emailconfirm/btn.png')} />
          </TouchableOpacity>
        </View>

      </Container>
    );
  }
}

