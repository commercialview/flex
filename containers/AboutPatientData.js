import React, {Component} from 'react';
import {View,Image, Text, TouchableOpacity, AsyncStorage, ScrollView} from 'react-native';
import {DatePicker, Picker, Container, Item, Input } from 'native-base';
import {Icon } from 'native-base';
import {Actions} from 'react-native-router-flux';

import HeaderV from '../components/f_HeaderV.js';

type Props = {};
export default class AboutPatientData extends Component<Props> {

    constructor(props) {

        super(props);
        
        this.state = {
            selected: "key0",
            name: '',
            lastName:'',
            relationship: '',
            phone:'',
            pname: '',
            plastName: '',
            calle: '',
            altura: '',
            ciudad: '',
            provincia: ''
          };

        var _this = this;

        AsyncStorage.getItem("Flexer:userDataAboutYou").then((value) => {
            if (value !== null) {
              let data = JSON.parse(value);
            
              _this.setState({
                name: data.name,
                lastName: data.lastName,
                selected: data.relationship,
                phone: data.phone
              });
      
            }
          });

          AsyncStorage.getItem("Flexer:userDataAboutYouPatient").then((value) => {
            if (value !== null) {
              let data = JSON.parse(value);

              console.log(data);
            
              _this.setState({
                pname: data.pname,
                plastName: data.plastName,
              });
      
            }
          });


          AsyncStorage.getItem("Flexer:userDataAboutYouPatientAddress").then((value) => {
            if (value !== null) {
              let data = JSON.parse(value);
              _this.setState({
                calle: data.calle,
                altura: data.altura,
                ciudad: data.ciudad ,
                provincia: data.provincia
              });
            }
          });

          
      }

      setDate(newDate) {
        this.setState({ chosenDate: newDate });
      }

      onValueChange(value: string) {
        this.setState({
          selected: value
        });
      }

  render() {
    return (
      <Container style={{backgroundColor:'#F7F7F7'}}>
        <HeaderV  back={true} />


        <ScrollView style={{paddingBottom:20}}>


        <View style={{paddingTop:20, paddingLeft:20, paddingRight:20}}>
          <Text style={{paddingLeft:2, fontSize:20, paddingTop:2, fontFamily:'SourceSansPro-Light',color:'#8DAC4A'}}>Acerca de vos (no del paciente)</Text>

          <Item style={{marginTop:10, padding:0, borderColor:'transparent'}}>
              <Input value={this.state.name} style={{fontSize:14, fontFamily:'SourceSansPro-Light', backgroundColor:'white',borderRadius:10}} placeholder="Juan" />
          </Item>
            <Item style={{marginTop:10, padding:0, borderColor:'transparent'}}>
              <Input value={this.state.lastName} style={{fontSize:14, fontFamily:'SourceSansPro-Light', backgroundColor:'white',borderRadius:10}} placeholder="Pérez" />
          </Item>



          <Text style={{paddingLeft:2, fontSize:18, paddingTop:10, fontFamily:'SourceSansPro-Light',color:'#8DAC4A'}}>Relación con el paciente</Text>

          <Picker
              mode="dropdown"
              iosHeader="Select your SIM"
              iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#8DAC4A", fontSize: 25 }} />}
              style={{ width: undefined, borderRadius:10 }}
              selectedValue={this.state.selected}
              itemStyle={{
                backgroundColor:'white',borderRadius:10
              }}
              itemTextStyle={{ color:'red', backgroundColor:'white',borderRadius:10
            }}

              onValueChange={this.onValueChange.bind(this)}
            >
              <Picker.Item label="Madre" value="key0" />
              <Picker.Item label="Padre" value="key1" />
              <Picker.Item label="Definir el resto" value="key2" />
            </Picker>

          <Text  style={{paddingLeft:2, fontSize:18, paddingTop:10, fontFamily:'SourceSansPro-Light',color:'#8DAC4A'}}>Número de teléfono</Text>

          <Item style={{marginTop:10, padding:0, borderColor:'transparent'}}>
              <Input value={this.state.phone} style={{fontSize:14, fontFamily:'SourceSansPro-Light', backgroundColor:'white',borderRadius:10}} placeholder="54 91136905203" />
          </Item>

        </View>


        <View style={{paddingTop:20, paddingLeft:20, paddingRight:20}}>
          <Text style={{paddingLeft:2, fontSize:20, paddingTop:2, fontFamily:'SourceSansPro-Light',color:'#8DAC4A'}}>Acerca del paciente</Text>

          <Item style={{marginTop:10, padding:0, borderColor:'transparent'}}>
              <Input value={this.state.pname} style={{fontSize:14, fontFamily:'SourceSansPro-Light', backgroundColor:'white',borderRadius:10}} placeholder="Nombre" />
          </Item>
            <Item style={{marginTop:10, padding:0, borderColor:'transparent'}}>
              <Input value={this.state.plastName} style={{fontSize:14, fontFamily:'SourceSansPro-Light', backgroundColor:'white',borderRadius:10}} placeholder="Apellido" />
          </Item>

          <Text style={{paddingLeft:2, fontSize:18, paddingTop:10, fontFamily:'SourceSansPro-Light',color:'#8DAC4A'}}>Fecha de nacimiento de paciente</Text>


          <DatePicker
            defaultDate={new Date(2018, 4, 4)}
            minimumDate={new Date(2018, 1, 1)}
            maximumDate={new Date(2018, 12, 31)}
            locale={"en"}
            timeZoneOffsetInMinutes={undefined}
            modalTransparent={false}
            animationType={"fade"}
            androidMode={"default"}
            placeHolderText="Select date..."
            placeHolderTextStyle={{ marginTop:10, fontFamily:'SourceSansPro-Light', backgroundColor:'white', borderRadius:10, color:'#7B7B7B' }}
            textStyle={{ marginTop:10, fontFamily:'SourceSansPro-Light', backgroundColor:'white', borderRadius:10, color:'#7B7B7B' }}
            onDateChange={this.setDate}
            disabled={false}
            />
        </View>


        <View style={{paddingTop:20, paddingLeft:20, paddingRight:20}}>
          <Text style={{paddingLeft:2, fontSize:20, paddingTop:2, fontFamily:'SourceSansPro-Light',color:'#8DAC4A'}}>Dirección del paciente</Text>

          <Item style={{marginTop:10, padding:0, borderColor:'transparent'}}>
              <Input  value={this.state.calle}  style={{fontSize:14, fontFamily:'SourceSansPro-Light', backgroundColor:'white',borderRadius:10}} placeholder="Calle" />
          </Item>
            <Item style={{marginTop:10, padding:0, borderColor:'transparent'}}>
              <Input  value={this.state.altura} style={{fontSize:14, fontFamily:'SourceSansPro-Light', backgroundColor:'white',borderRadius:10}} placeholder="Altura" />
          </Item>
          <Item style={{marginTop:10, padding:0, borderColor:'transparent'}}>
              <Input  value={this.state.ciudad}  style={{fontSize:14, fontFamily:'SourceSansPro-Light', backgroundColor:'white',borderRadius:10}} placeholder="Ciudad" />
          </Item>
            <Item style={{marginTop:10, padding:0, borderColor:'transparent'}}>
              <Input value={this.state.provincia}   style={{fontSize:14, fontFamily:'SourceSansPro-Light', backgroundColor:'white',borderRadius:10}} placeholder="Provincia" />
          </Item>


        </View>

        </ScrollView>


      </Container>
    );
  }
}

