import React, {Component} from 'react';
import {Alert,View,Image, Text, TouchableOpacity, AsyncStorage} from 'react-native';
import { Form, Container, Item, Input } from 'native-base';
import {Actions} from 'react-native-router-flux';
import { LoginManager, AccessToken, GraphRequest,
  GraphRequestManager } from "react-native-fbsdk";
import Spinner from 'react-native-loading-spinner-overlay';
import { GoogleSignin, statusCodes } from 'react-native-google-signin';

import Header from '../components/f_Header.js';

type Props = {};
export default class LoginLogin extends Component<Props> {

  state = {
    email: '',
    password: '',
    source:'',
    loading: false
  };


// Somewhere in your code
googleLogin = async () => {
  GoogleSignin.configure();

  try {
    await GoogleSignin.hasPlayServices();
    const userInfo = await GoogleSignin.signIn();

    var login = {
      status: true,
      fbid: '',
      name: userInfo.user.name,
      picture: userInfo.user.photo
    }

    AsyncStorage.setItem("Flexer:login", JSON.stringify(login));

    Actions.registeraboutyou({type:'reset'});

  } catch (error) {

    var login = {
      status: true,
      fbid: '',
      name: '',
      picture: 'https://asapct.org/wp-content/uploads/2016/02/blank-avatar.jpg'
    }

    AsyncStorage.setItem("Flexer:login", JSON.stringify(login));

    Actions.registeraboutyou({type:'reset'});

    if (error.code === statusCodes.SIGN_IN_CANCELLED) {
      // user cancelled the login flow
    } else if (error.code === statusCodes.IN_PROGRESS) {
      // operation (f.e. sign in) is in progress already
    } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      // play services not available or outdated
    } else {
      // some other error happened
    }
  }

};


  _responseInfoCallback(error: ?Object, result: ?Object) {
    if (error) {
      console.log('Error fetching data: ' + error.toString());
    } else {
      console.log('Success fetching data: ' + result.toString());
    }
  }



  fbLogin(){
    LoginManager.logInWithReadPermissions(["public_profile"]).then(
      function(result) {

        if (result.isCancelled) {
          console.log("Login cancelled");
        } else {
          
          AccessToken.getCurrentAccessToken().then((data) => {

            let myAccessToken = data.accessToken.toString();
            let GetInfoUSer = () => {

              return new Promise((resolve, reject) => {
              
                  const infoRequest = new GraphRequest('/me', null, ((error, result) => {
                      if (error) {
                          reject(error)
                      } else {
                         resolve(result)
                      }
                  }))
              
                  new GraphRequestManager().addRequest(infoRequest).start();
              
                })
              }
              
            GetInfoUSer().then(response => {

                var login = {
                  status: true,
                  fbid: response.id,
                  name: response.name,
                  picture: 'https://graph.facebook.com/' + response.id + '/picture?type=normal'
                }

                AsyncStorage.setItem("Flexer:login", JSON.stringify(login));

                Actions.registeraboutyou({type:'reset'});

            }).catch(error => {
                console.log(error)
            })
        });

        }
      },
      function(error) {
        Actions.registeraboutyou({type:'reset'});
        console.log("Login fail with error: " + error);
      }
    );
  }



  
  login(){

    this.setState({loading:true});
      var form = new FormData();  
      form.append('email', this.state.email);
      form.append('password', this.state.password);
      form.append('source', 'normal');
    
      fetch('http://200.69.211.17/api/users/login', {
        method: 'POST',
        body: form,
    }).then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
            this.setState({loading:false});
            if (responseJson.STATE == "ERROR"){
            Alert.alert(
              'Flexer',
              responseJson.MESSAGE,
              [
                {text: 'Aceptar', onPress: () => console.log('OK Pressed')},
              ],
              {cancelable: false},
            );
          }else{
              if (responseJson.MESSAGE == 'Usuario a confirmar'){
                Actions.emailconfirm({type:'reset', email:this.state.email})
              }else{
                
                var login = {
                  status: true,
                  fbid: 0,
                  name: '',
                  picture: 'https://asapct.org/wp-content/uploads/2016/02/blank-avatar.jpg',
                  id: responseJson.id_user
                }

                AsyncStorage.setItem("Flexer:login", JSON.stringify(login));

                Actions.registeraboutyou({type:'reset'});
              }
          }
        })
        .catch((error) => {
          console.error(error);
      });
  }

  render() {
    return (
      <Container style={{backgroundColor:'#A18CD1'}}>
              <Spinner
          visible={this.state.loading}
          textContent={'Cargando...'} />

        <Header back={true} bgcolor="#A18CD1" />
    
        <View style={{paddingTop:20, paddingLeft:20, paddingRight:20}}>
          <Text style={{paddingLeft:2, fontSize:20, paddingTop:2, fontFamily:'SourceSansPro-Light',color:'white'}}>Inicio de sesión</Text>

          <Item style={{marginTop:10, padding:0, borderColor:'transparent'}}>
              <Input onChangeText={(text) => this.setState({email:text})} style={{fontSize:14, fontFamily:'SourceSansPro-Light', backgroundColor:'white',borderRadius:10}} placeholder="email@dominio.com" />
            </Item>
          <Item style={{marginTop:10, padding:0, borderColor:'transparent'}}>
              <Input onChangeText={(text) => this.setState({password:text})} secureTextEntry={true}  style={{fontSize:14, fontFamily:'SourceSansPro-Light', margin:0, backgroundColor:'white',borderRadius:10}} placeholder="*******" />
          </Item>
        </View>

        <View>
          <Text style={{paddingTop:15, fontFamily:'SourceSansPro-Light',color:'white', alignSelf:'center'}}>o conectate con...</Text>
        </View>

        <View style={{height:100}}>
          <View style={{flex:1, flexDirection:'row', alignItems:'center', justifyContent: 'center'}}>
              <TouchableOpacity onPress={ () => this.fbLogin()  }>
                <Image style={{alignSelf:'center',width:65, height:62}} source={require('../res/login/facebook.png')} />  
              </TouchableOpacity>
              <TouchableOpacity onPress={ () => this.googleLogin()  }>
                <Image style={{alignSelf:'center',width:60, height:60}} source={require('../res/login/twitter.png')} />
                </TouchableOpacity>
                          </View>
        </View>

        <View style={{paddingTop:20, paddingLeft:20, paddingRight:20}}>
          <TouchableOpacity onPress={() => this.login() } style={{position:'absolute', right: 10, width:80, height:80}}>
           <Image style={{position:'absolute', right: 10, width:80, height:80}} source={require('../res/login/btn.png')} />
          </TouchableOpacity>
        </View>

      </Container>
    );
  }
}

