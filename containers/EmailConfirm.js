import React, {Component} from 'react';
import {View,Image, TouchableOpacity} from 'react-native';
import { Container, Content, Button, Text } from 'native-base';
import {Actions} from 'react-native-router-flux';

import HeaderV from '../components/f_HeaderV.js';

type Props = {};
export default class EmailConfirm extends Component<Props> {

  constructor(props){
    super(props);
  }
  render() {
    return (
      <Container style={{backgroundColor:'#F7F7F7'}}>
        <HeaderV />
        
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Image style={{alignSelf:'center',width:179, height:174}} source={require('../res/emailconfirm/img.png')} />
          <Text style={{fontSize:16, fontFamily:'SourceSansPro-Light',paddingTop:25, textAlign:'center', width: 240, color:'#4D4D4D'}}>Por favor confirmá la información que completaste clickeando en el link que te acabamos de enviar a tu email {this.props.email}.</Text>
          <Button onPress={ () => {Actions.registeraboutyou({type:'reset'})}} style={{borderRadius: 10, marginTop:20, alignSelf:'center', backgroundColor:'#7EAD41'}}>
              <Text uppercase={false} style={{fontFamily:'SourceSansPro-Light'}}>Entendido, empecemos!</Text>
          </Button>
        </View>

      </Container>
    );
  }
}

