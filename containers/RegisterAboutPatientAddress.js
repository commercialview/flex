import React, {Component} from 'react';
import {View,Image, Text, TouchableOpacity, AsyncStorage} from 'react-native';
import {DatePicker, Picker, Container, Item, Input } from 'native-base';
import {Actions} from 'react-native-router-flux';

import HeaderV from '../components/f_HeaderV.js';

type Props = {};
export default class RegisterAboutPatientAddress extends Component<Props> {

  constructor(props) {
    super(props);

    this.state = { 
      calle: '',
      altura: '',
      ciudad: '',
      provincia: ''
    };
    
  }


  saveInDB(){
    // Actions.homefull({type:'reset'});

    var _this = this;

    var patientAddress = {
      calle: this.state.calle,
      altura: this.state.altura,
      ciudad: this.state.ciudad ,
      provincia: this.state.provincia
    }
    
    AsyncStorage.getItem("Flexer:userDataAboutYou").then((value) => {
      if (value !== null) {
        let data = JSON.parse(value);
      
        _this.setState({
          name: data.name,
          lastName: data.lastName,
          selected: data.relationship,
          phone: data.phone
        });
      }


      AsyncStorage.getItem("Flexer:userDataAboutYouPatient").then((value) => {
        if (value !== null) {
          let data = JSON.parse(value);
        
          _this.setState({
            pname: data.pname,
            plastName: data.plastName,
          });
  
        }
      });

      this.setState({loading:true});
      var form = new FormData();  
      form.append('email', this.state.email);
      form.append('password', this.state.password);
      form.append('source', 'normal');
    
      fetch('http://200.69.211.17/api/contactos_relacionados ', {
        method: 'POST',
        body: form,
    }).then((response) => response.json())
        .then((responseJson) => {

  
        })
        .catch((error) => {
          console.error(error);
      });


    });


  }
  
  save(){
      var aboutYou = {
        calle: this.state.calle,
        altura: this.state.altura,
        ciudad: this.state.ciudad ,
        provincia: this.state.provincia
      }

      AsyncStorage.setItem("Flexer:userDataAboutYouPatientAddress", JSON.stringify(aboutYou));
  }

  render() {
    return (
      <Container style={{backgroundColor:'#F7F7F7'}}>
        <HeaderV  />

        <View style={{paddingTop:20, paddingLeft:20, paddingRight:20}}>
          <Text style={{paddingLeft:2, fontSize:20, paddingTop:2, fontFamily:'SourceSansPro-Light',color:'#8DAC4A'}}>Dirección del paciente</Text>

          <Item style={{marginTop:10, padding:0, borderColor:'transparent'}}>
              <Input onChangeText={(text) => this.setState({calle:text})}  style={{fontSize:14, fontFamily:'SourceSansPro-Light', backgroundColor:'white',borderRadius:10}} placeholder="Calle" />
          </Item>
            <Item style={{marginTop:10, padding:0, borderColor:'transparent'}}>
              <Input onChangeText={(text) => this.setState({altura:text})}  style={{fontSize:14, fontFamily:'SourceSansPro-Light', backgroundColor:'white',borderRadius:10}} placeholder="Altura" />
          </Item>
          <Item style={{marginTop:10, padding:0, borderColor:'transparent'}}>
              <Input onChangeText={(text) => this.setState({ciudad:text})}  style={{fontSize:14, fontFamily:'SourceSansPro-Light', backgroundColor:'white',borderRadius:10}} placeholder="Ciudad" />
          </Item>
            <Item style={{marginTop:10, padding:0, borderColor:'transparent'}}>
              <Input onChangeText={(text) => this.setState({provincia:text})}  style={{fontSize:14, fontFamily:'SourceSansPro-Light', backgroundColor:'white',borderRadius:10}} placeholder="Provincia" />
          </Item>


        </View>

        <View style={{marginTop:40, paddingLeft:20, paddingRight:20}}>
          <TouchableOpacity onPress={() => this.save() } style={{position:'absolute', right: 10, width:80, height:80}}>
           <Image style={{position:'absolute', right: 10, width:70, height:70}} source={require('../res/emailconfirm/btn.png')} />
          </TouchableOpacity>
        </View>

      </Container>
    );
  }
}

