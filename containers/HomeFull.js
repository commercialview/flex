import React, {Component} from 'react';
import {Alert,View,Image, Text, TouchableOpacity, Dimensions, AsyncStorage} from 'react-native';
import { Button, Content, Icon, Left, Container, Item, Input } from 'native-base';
import {Actions} from 'react-native-router-flux';
import { Drawer } from 'native-base';

import HeaderV from '../components/f_HeaderV.js';
import SideBar from '../components/Sidebar.js';
type Props = {};
export default class HomeFull extends Component<Props> {

  state = {
    id:0,
    loading: false,
    name: '',
    picture: 'https://asapct.org/wp-content/uploads/2016/02/blank-avatar.jpg'
  };


  getLatestPreferences(){

    var _this = this;

    fetch('http://200.69.211.17/api/contactos_relacionados', {
      method: 'GET',
    }).then((response) => response.json())
        .then((responseJson) => {

          let found = false;
          responseJson.forEach( (e, k) => {
            if (e.id_usuario == _this.state.id){

              found = true;
              console.log(e);

              global.name       = e.nombre;
              global.last_name  = e.apellido;

              _this.setState({
                name: global.name,
                last_name: global.last_name.replace(global.name, ""),
              });

              if (e.vinculo.length < 1)
                Actions.registeraboutyou({type:'reset'});
              
            }
          });

          if (!found) Actions.registeraboutyou({type:'reset'});
        })
        .catch((error) => {
          console.error(error);
      });
  }


  componentDidMount(){
    var _this = this;

    AsyncStorage.getItem("Flexer:login").then((value) => {
      if (value !== null) {
        let data = JSON.parse(value);
        
        _this.setState({
          name: data.name,
          picture: data.picture,
          id: data.id
        });

        _this.getLatestPreferences();

      }
    });

  }

  render() {
    closeDrawer = () => {
        this.drawer._root.close()
      };
      openDrawer = () => {
        this.drawer._root.open()
      };

    return (
        <Drawer
        ref={(ref) => { this.drawer = ref; }}
        content={<SideBar navigator={this.navigator} />}>
 
        <Container style={{backgroundColor:'#F7F7F7'}}>
            <View style={{borderColor:'#B4B4B4',borderBottomWidth:0.4,paddingTop:20, paddingBottom:15, backgroundColor:'white'}}>
                <Left style={{position:'absolute',left:3,top:10}}>
                <Button onPress={ () => openDrawer()} transparent>
                    <Image style={{marginLeft:5,height:35, width:35}} source={require('../res/homefull/menu.png')} />
                </Button>
                </Left>   
                <Image style={{alignSelf:'center',width:128, height:31}} source={require('../res/logos/fndf_green.png')} />
            </View>

            <Content>
            <Image style={{marginTop:0.1,width:Dimensions.get("window").width, height:190}} source={require('../res/homefull/bg.png')} />
            <Image style={{left:10, top:-130, position:'relative', borderWidth:3, borderColor:'white',height:80,width:80, borderRadius: 6}} source={{uri: this.state.picture}} />
            <View style={{borderRadius:10, padding:10, backgroundColor:'#8DAC4B',position:'relative', top:-115, width: Dimensions.get("window").width - 30, alignSelf:'center'}}>
                <Text style={{fontFamily:'SourceSansPro-Light',fontSize:20, color:'white'}}>Hola {this.state.name}! Estamos para ayudarte :)</Text>
            </View>

            <View style={{top:-90, alignSelf:'center',width: Dimensions.get("window").width - 30}}>
                <Item style={{borderColor:'transparent'}}>
                <Icon  style={{position:'relative'}} active name='search' />
                <Input onChangeText={(text) => this.setState({email:text})} style={{fontSize:14, fontFamily:'SourceSansPro-Light', backgroundColor:'white',borderRadius:10}} placeholder="Quiero buscar..." />
                </Item>
            </View>   
            </Content>
        </Container>
      </Drawer>

    
    );
  }
}

