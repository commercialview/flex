import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import {Left, Button, Icon} from 'native-base';
import {Actions} from 'react-native-router-flux';

type Props = {};
export default class HeaderV extends Component<Props> {

  constructor(props){
    super(props);
  }

  render() {
    return (
        <View style={{borderColor:'#B4B4B4',borderBottomWidth:0.4,paddingTop:20, paddingBottom:15, backgroundColor:'white'}}>
          {this.props.back &&
              <Left style={{position:'absolute',left:3,top:10}}>
               <Button onPress={ () => Actions.pop() } transparent>
                   <Icon style={{color:'#7EAD41'}} name='arrow-back' />
                 </Button>
               </Left>     
          }

          {this.props.menu &&
              <Left style={{position:'absolute',left:3,top:10}}>
              <Button onPress={ () => this.props.menuClick} transparent>
                <Image style={{marginLeft:5,height:35, width:35}} source={require('../res/homefull/menu.png')} />
              </Button>
              </Left>   
          }

            <Image style={{alignSelf:'center',width:128, height:31}} source={require('../res/logos/fndf_green.png')} />
        </View>
    );
  }
}

