import React, {Component} from 'react';
import {AsyncStorage,View,Image, TouchableOpacity} from 'react-native';
import { Content, Container, Text, Input } from 'native-base';
import {Actions} from 'react-native-router-flux';
import { LoginManager } from "react-native-fbsdk";
import Spinner from 'react-native-loading-spinner-overlay';

import Header from '../components/f_Header.js';

type Props = {};
export default class Sidebar extends Component<Props> {

  state = {

  };

  logout(){
    _storeData = async () => {
      try {
        await AsyncStorage.removeItem("Flexer:login");
      } catch (error) {
       
      }
    };

    _storeData().then( () => {
      Actions.login({type:'reset'});
    })
  }

  render() {
    return (
      <Container style={{backgroundColor:'white'}}>
        <Content style={{padding:20}}>
            <Text style={{fontFamily:'SourceSansPro-Light', fontSize:24, color:'#8DAC4B', }}>Menú</Text>

            <View style={{marginTop:40}}>
              <TouchableOpacity onPress={ () => Actions.aboutpatientdata()}>
                <Text style={{marginLeft:10, fontFamily:'SourceSansPro-Light', fontSize:20, color:'black', }}>Ver datos</Text>
              </TouchableOpacity>
            </View>

            <View style={{marginTop:20}}>
              <TouchableOpacity onPress={ () => this.logout()}>
                <Text style={{marginLeft:10, fontFamily:'SourceSansPro-Light', fontSize:20, color:'black', }}>Cerrar sesión</Text>
              </TouchableOpacity>
            </View>

        </Content>
      </Container>
    );
  }
}

