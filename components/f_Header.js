import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import {Left, Button, Icon} from 'native-base';
import {Actions} from 'react-native-router-flux';

type Props = {};
export default class Header extends Component<Props> {

  constructor(props){
    super(props);
  }

  render() {
    return (
        <View style={{borderColor:'white',borderBottomWidth:0.4,paddingTop:20, paddingBottom:15, backgroundColor:this.props.bgcolor}}>
          {this.props.back &&
              <Left style={{position:'absolute',left:3,top:10}}>
               <Button onPress={ () => Actions.pop() } transparent>
                   <Icon style={{color:'white'}} name='arrow-back' />
                 </Button>
               </Left>     
          }


            <Image style={{alignSelf:'center',width:128, height:31}} source={require('../res/logos/fndf_white.png')} />
        </View>
    );
  }
}

