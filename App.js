import React, {Component} from 'react';
import SplashScreen from 'react-native-splash-screen';
import {Router, Stack, Scene, Actions} from 'react-native-router-flux';
import {AsyncStorage} from 'react-native';
import Home   from './containers/Home.js';
import Login  from './containers/Login.js';
import LoginLogin  from './containers/LoginLogin.js';
import EmailConfirm   from './containers/EmailConfirm';
import RegisterAboutYou from './containers/RegisterAboutYou.js';
import RegisterAboutPatient from './containers/RegisterAboutPatient.js';
import RegisterAboutPatientAddress from './containers/RegisterAboutPatientAddress.js';
import HomeFull from './containers/HomeFull.js';
import AboutPatientData from './containers/AboutPatientData';


type Props = {};
export default class App extends Component<Props> {

  componentDidMount() {
      SplashScreen.show();

      AsyncStorage.getItem("Flexer:login").then((value) => {
        if (value !== null) {
  
          Actions.homefull({type:'reset'});
  
        }
      });

      setTimeout(() => {
        SplashScreen.hide();

      },3000)
  }

  render() {
    return (
      <Router>
        <Stack key="root">
          <Scene key="home" hideNavBar component={Home} initial={true} />
          <Scene key="homefull" hideNavBar  component={HomeFull}/>
          <Scene key="login" hideNavBar component={Login}/>
          <Scene key="loginlogin" hideNavBar component={LoginLogin}/>
          <Scene key="emailconfirm" hideNavBar component={EmailConfirm}/>
          <Scene key="registeraboutyou" hideNavBar component={RegisterAboutYou}/>
          <Scene key="registeraboutpatient" hideNavBar component={RegisterAboutPatient}/>
          <Scene key="registeraboutpatientaddress" hideNavBar component={RegisterAboutPatientAddress}/>
          <Scene key="aboutpatientdata" hideNavBar component={AboutPatientData}/>
        </Stack>
      </Router>
    );
  }
}

